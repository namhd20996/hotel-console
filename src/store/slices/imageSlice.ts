import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { notificationController } from '@app/controllers/notificationController';
import { uploadImages, deleteImage } from '@app/api/images.api';

export interface ImageState {
    loading: boolean;
    images: any[];
}

const initialState: ImageState = {
    loading: false,
    images: [],
};

export const uploadImage = createAsyncThunk(
    'image/uploadImage',
    async (payload: any, { rejectWithValue }) => {
        try {
            const response = await uploadImages(payload);
            return response;
        }
        catch (error: any) {
            notificationController.error({
                message: payload.t('notification.MSG_100'),
                description:
                    error.options.message,
                placement: 'topRight',
            });
            return rejectWithValue(error.response)
        }
    }
);

export const deleteImageByName = createAsyncThunk(
    'image/deleteImage',
    async (id: string, { rejectWithValue }) => {
        try {
            const response = await deleteImage(id);
            return response;
        }
        catch (error: any) {
            notificationController.error({
                message: 'Error',
                description:
                    error.options.message,
                placement: 'topRight',
            });
            return rejectWithValue(error.response)
        }
    }
);

const imageSlice = createSlice({
    name: 'image',
    initialState,
    reducers: {
        setLoading: (state, action) => {
            state.loading = action.payload;
        },
    },
    extraReducers: (builder) => {
        builder.addCase(uploadImage.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(uploadImage.fulfilled, (state, action) => {
            state.loading = false;
            state.images = action.payload;
        });
        builder.addCase(uploadImage.rejected, (state) => {
            state.loading = false;
        });
        builder.addCase(deleteImageByName.pending, (state) => {
            state.loading = true;
        });
        builder.addCase(deleteImageByName.fulfilled, (state, action) => {
            state.loading = false;
            state.images = action.payload;
        });
        builder.addCase(deleteImageByName.rejected, (state) => {
            state.loading = false;
        });
    },
});

export const { setLoading } = imageSlice.actions;
export default imageSlice.reducer;