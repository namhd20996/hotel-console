import { httpApi } from '@app/api/http.api';

export interface ImageRequest {
    targetId: string;
    images: string[];
}

export const uploadImages = async (payload: ImageRequest): Promise<any> => {
    return await httpApi.post(`/admin/v1/images`, payload)
        .then(({ data }) => data)
}

export const deleteImage = async (name: string): Promise<any> => {
    return await httpApi.delete(`/admin/v1/images/${name}`)
        .then(({ data }) => data)
}