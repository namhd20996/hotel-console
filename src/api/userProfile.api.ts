import axios from 'axios';
import { UserModel } from '@app/domain/UserModel';
import { httpApi } from './http.api';

export interface ProfileRequest {
    token: String,
}

export interface ProfileResponse {
    user: UserModel;
}

export const getProfileUser = (): Promise<ProfileResponse | any> =>
    httpApi.get<ProfileResponse>('/admin/v1/user/jwt-token').then(({ data }) => data)

export const getUserPermission = (): Promise<any> =>
    httpApi.get<ProfileResponse>('/admin/v1/user/permissions-jwt-token').then(({ data }) => data)

