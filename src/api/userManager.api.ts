import { httpApi } from './http.api';
import { UserModel } from '@app/domain/UserModel';

export interface UsersRequest {
    token: String,
}

export interface UsersResponse {
    users: UserModel[];
}

export interface UserResponse {
    user: UserModel;
}

export const getUsers = (payload: any): Promise<UsersResponse | any> =>
    httpApi.get<UsersResponse>(`/admin/v1/user?currentPage=${payload.currentPage}&limitPage=${payload.limitPage}${payload.status ? `&status=${payload.status}` : ''}`)
        .then(({ data }) => data)

export const getUser = (payload: any): Promise<UserResponse | any> =>
    httpApi.get<UserResponse>(`/admin/v1/user/${payload.id}`)
        .then(({ data }) => data)

export const addUser = (payload: any): Promise<UserResponse | any> =>
    httpApi.post<UserResponse>('/admin/v1/user', payload.values)
        .then(({ data }) => data)

export const activeUser = (payload: any): Promise<UserResponse | any> =>
    httpApi.post<UserResponse>(`/admin/v1/user/${payload.id}`)
        .then(({ data }) => data)

export const deleteUser = (payload: any): Promise<UserResponse | any> =>
    httpApi.delete<UserResponse>(`/admin/v1/user/${payload.id}`)
        .then(({ data }) => data)