import React from 'react';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { doGetRooms, setRoomId } from '@app/store/slices/roomSlice';
import FilterRoomsPage from './uiComponentsPages/forms/FilterRoomsPage';
import RoomList from './uiComponentsPages/dataDisplay/RoomList';
import AddRoom from './uiComponentsPages/forms/AddRoom';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';

export const Rooms: React.FC = () => {
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const navigate = useNavigate();
    const { id } = useParams();
    const { rooms, roomId, loading, totalPages, currentPage, limitPage, languageCode, roomName } = useSelector((state: any) => state.room);
    const [isModalOpenRoom, setIsModalOpenRoom] = React.useState(false);

    React.useEffect(() => {
        if (id) {
            dispatch(doGetRooms({ values: { hotelId: id, currentPage, limitPage, languageCode, roomName }, t }));
        }
    }, [dispatch, id, currentPage, limitPage, languageCode]);

    return (
        <>
            <h3 className='text-2xl font-bold capitalize'>Room List</h3>
            <div className="flex flex-wrap md:flex-nowrap items-center my-3">
                <div className="flex flex-wrap md:flex-nowrap items-center w-full">
                    <FilterRoomsPage hotelId={id} />
                    <div className="w-full flex flex-nowrap gap-5">
                        <div className='ml-auto'>
                            <PermissionChecker
                                hasPermissions={['room_create_info']}
                            >
                                <BaseButton
                                    type="primary"
                                    onClick={() => {
                                        dispatch(setRoomId(''));
                                        setIsModalOpenRoom(true)
                                    }
                                    }
                                >
                                    Add room
                                </BaseButton>
                            </PermissionChecker>
                        </div>
                    </div>
                </div>
            </div>
            <RoomList
                isModalOpen={isModalOpenRoom}
                setIsModalOpen={setIsModalOpenRoom}
                hotelId={id}
            />
            <AddRoom
                isModalOpen={isModalOpenRoom}
                setIsModalOpen={setIsModalOpenRoom}
                roomId={roomId}
                hotelId={id}
                language={languageCode}
            />
        </>
    );
};

export default Rooms;
