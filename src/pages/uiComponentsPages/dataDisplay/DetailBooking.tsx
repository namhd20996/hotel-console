import React from 'react';
import { Modal, Descriptions, Badge } from 'antd';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

interface DetailBookingProps {
    isModalOpen?: boolean;
    setIsModalOpen?: (state: boolean) => void;
}

export const DetailBooking: React.FC<DetailBookingProps> = ({ isModalOpen, setIsModalOpen }) => {
    const { t } = useTranslation();
    const { booking } = useSelector((state: any) => state.booking);

    const showModal = () => {
        if (setIsModalOpen) {
            setIsModalOpen(true);
        }
    };

    const handleOk = () => {
        if (setIsModalOpen) {
            setIsModalOpen(false);
        }
    };

    const handleCancel = () => {
        if (setIsModalOpen) {
            setIsModalOpen(false);
        }
    };

    return (
        <Modal
            title={"Booking Detail"}
            open={isModalOpen}
            onCancel={handleCancel}
            footer={null}
            width={1300}
        >
            <Descriptions bordered>
                <Descriptions.Item label="Thông tin" span={4}>
                    <div>
                        <p><span className='font-semibold'>Mã đặt phòng: </span>{booking?.bookingCode}</p>
                        <p><span className='font-semibold'>Tên khách sạn: </span>{booking?.hotelName}</p>
                        <p><span className='font-semibold'>Địa chỉ: </span>{booking?.address}</p>
                        <p><span className='font-semibold'>Số điện thoại: </span>{booking?.phoneNumberHotel}</p>
                    </div>
                </Descriptions.Item>
                <Descriptions.Item label="Thông tin lưu trú" span={4}>
                    <div className='flex justify-start items-center gap-8'>
                        {
                            booking.guestInfos?.map((guest: any, index: number) => {
                                return (
                                    <div>
                                        <p><span className='font-semibold'>Họ tên: </span>{guest?.name}</p>
                                        <p><span className='font-semibold'>Điện thoại: </span>{guest?.phoneNumber}</p>
                                        <p><span className='font-semibold'>Email: </span>{guest?.email}</p>
                                    </div>
                                )
                            })
                        }
                    </div>
                </Descriptions.Item>
                <Descriptions.Item label="Phòng đặt" span={4}>
                    {
                        booking.roomInfos?.map((room: any, index: number) => {
                            return (
                                <div>
                                    <span className='font-semibold'>Thông tin phòng:</span>
                                    <br />
                                    <p><span className='font-semibold'>Sức chứa: </span>{room?.quantityAdult && room?.quantityAdult} người lớn, {room?.quantityChildren} trẻ em</p>
                                    <p><span className='font-semibold'>Diện tích: </span>{room?.acreage}m2</p>
                                    <p><span className='font-semibold'>Loại giường: </span>{room?.extraBedQuantity + room?.singleBedQuantity} giường ({room?.extraBedQuantity} đôi, {room?.singleBedQuantity} đơn)</p>
                                </div>
                            )
                        })
                    }
                </Descriptions.Item>
            </Descriptions>
        </Modal>
    );
};

export default DetailBooking;
