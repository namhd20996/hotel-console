import React from 'react';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { Table, Switch, message } from 'antd';
import { RoomModel } from '@app/domain/RoomModel';
import { doGetRooms, doChangeRoomStatus, doDeleteRoom, setLanguageCode, setRoomId, setRoomIdPolicy } from '@app/store/slices/roomSlice';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { BasePopconfirm } from '@app/components/common/BasePopconfirm/BasePopconfirm';
import { FormRoomPolicy } from '../forms/FormRoomPolicy';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';


interface RoomListProps {
    isModalOpen: boolean;
    setIsModalOpen: (state: boolean) => void;
    hotelId?: string
}

export const RoomList: React.FC<RoomListProps> = ({ isModalOpen, setIsModalOpen, hotelId }) => {
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const { rooms, roomId, roomIdPolicy, loading, totalPages, currentPage, limitPage, languageCode, roomName } = useSelector((state: any) => state.room);
    const [data, setData] = React.useState<any[]>([]);
    const [isModalOpenRoomPolicy, setIsModalOpenRoomPolicy] = React.useState(false);
    const queryParams = new URLSearchParams(location.search);
    const language = queryParams.get('language');
    const formatter = new Intl.NumberFormat('vi-VN', {
        style: 'currency',
        currency: 'VND',
        minimumFractionDigits: 0,
        currencyDisplay: 'symbol',
    });

    React.useEffect(() => {
        dispatch(setLanguageCode(language));
        setData(rooms.map((room: any) => {
            const roomInfo = room.roomInfos.find((info: any) => info.languageCode === languageCode);
            return {
                ...room,
                key: room.roomId,
                roomId: room.roomId,
                roomName: roomInfo?.name,
                roomType: room.type.code,
                status: room.status,
                description: roomInfo?.description,
                totalBed: room.singleBedQuantity + room.twinBedQuantity + room.extraBedQuantity,
                acreage: room.acreage,
                capacity: room.capacity,
                quantityRoom: room.quantityRoom,
                price: formatter.format(room.price),
            }
        }));
    }, [rooms]);

    const confirm = (e: React.MouseEvent<HTMLElement>, roomId: string) => {
        dispatch(doDeleteRoom({ values: { roomId }, t }));
    };

    const columns = [
        {
            title: 'Room Name',
            dataIndex: 'roomName',
            key: 'roomName',
        },
        {
            title: 'Room Type',
            dataIndex: 'roomType',
            key: 'roomType',
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Information Room',
            dataIndex: 'informationRoom',
            key: 'informationRoom',
            render: (_: any, item: any) => {
                return (
                    <div className='flex flex-col justify-start items-start'>
                        <p>Acreage: {item.acreage}m2</p>
                        <p>Capacity: {item.capacity}</p>
                    </div>
                )
            },
        },
        {
            title: 'Acreage',
            dataIndex: 'acreage',
            key: 'acreage',
        },
        {
            title: 'Quantity Room',
            dataIndex: 'quantityRoom',
            key: 'quantityRoom',
        },
        {
            title: 'Price',
            dataIndex: 'price',
            key: 'price',
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            render: (status: string, record: RoomModel) =>
                <div className="flex justify-between">
                    {status === "ACTIVE" ? (
                        <span className="text-base font-bold text-[#52c41a] dark:text-gray-300">{t('user.active')}</span>
                    ) : (
                        <span className="text-base font-bold text-[#f5222d] dark:text-gray-300">{t('user.in_active')}</span>
                    )}
                    <PermissionChecker
                        hasPermissions={['room_update_status']}
                    >
                        <Switch
                            className={`ml-2 ${record.status === "ACTIVE" ? 'bg-blue-500' : 'bg-gray-500'}`}
                            checked={record.status === "ACTIVE" ? true : false}
                            loading={loading}
                            onChange={(e) => dispatch(doChangeRoomStatus({
                                values: {
                                    roomId: record.roomId,
                                    status: e ? "ACTIVE" : "UN_ACTIVE"
                                },
                                t
                            }))}
                        />
                    </PermissionChecker>
                </div>
        },
        {
            title: 'Function',
            dataIndex: 'function',
            key: 'function',
            render: (_: any, item: any) => (
                <div className="flex justify-center items-center gap-4">
                    <BaseButton
                        type="ghost"
                        onClick={() => {
                            dispatch(setRoomId(item.roomId));
                            setIsModalOpen(true);
                        }}
                    >
                        Detail
                    </BaseButton>
                    <PermissionChecker
                        hasPermissions={['room_delete']}
                    >
                        <BasePopconfirm
                            title="Are you sure delete the room?"
                            onConfirm={(e?: React.MouseEvent<HTMLElement>) => {
                                if (e) {
                                    confirm(e, item.roomId);
                                }
                            }}
                            okText="Yes"
                            cancelText="No"
                        >
                            <BaseButton type="primary">Delete</BaseButton>
                        </BasePopconfirm>
                    </PermissionChecker>
                    <PermissionChecker
                        hasPermissions={['room_view_policy', 'room_create_policy', 'room_update_policy']}
                    >
                        <BaseButton
                            type="ghost"
                            onClick={() => {
                                setIsModalOpenRoomPolicy(true);
                                dispatch(setRoomIdPolicy(item.roomId));
                            }}
                        >
                            Policy
                        </BaseButton>
                    </PermissionChecker>
                </div>
            ),
        },
    ];
    return (
        <>
            <Table
                dataSource={data}
                columns={columns}
                loading={loading}
                pagination={{
                    showSizeChanger: true,
                    pageSizeOptions: ['5', '10', '20', '30', '40', '50'],
                    defaultPageSize: limitPage,
                    defaultCurrent: currentPage,
                    total: totalPages * limitPage,
                    onChange: (page: number, pageSize: number) => {
                        dispatch(doGetRooms({ values: { hotelId, currentPage: page, limitPage: pageSize, languageCode, roomName }, t }));
                    },
                    showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
                }}
            />
            <FormRoomPolicy
                isModalOpen={isModalOpenRoomPolicy}
                setIsModalOpen={setIsModalOpenRoomPolicy}
                roomId={roomIdPolicy}
            />
        </>
    );
};

export default RoomList;
