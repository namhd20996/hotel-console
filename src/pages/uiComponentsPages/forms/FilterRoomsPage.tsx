import React from 'react';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { Form, Input, Select } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { doGetRooms, setRoomName, setCurrentPage } from '@app/store/slices/roomSlice';
import { useTranslation } from 'react-i18next';

interface FilterRoomsPageProps {
    hotelId?: string;
}

type FieldType = {
    roomName: string;

};

export const FilterRoomsPage: React.FC<FilterRoomsPageProps> = ({ hotelId }) => {

    const [form] = Form.useForm();
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const { rooms, loading, totalPages, currentPage, limitPage, languageCode, roomName } = useSelector((state: any) => state.room);

    React.useEffect(() => {
        form.setFieldsValue({
            roomName: '',
        });
    }, [form]);

    const onFinish = (values: any) => {
        // dispatch(setCurrentPage(1));
        // dispatch(setRoomName(values.roomName));
        dispatch(doGetRooms({ values: { hotelId, currentPage: 1, limitPage, languageCode, roomName: values.roomName }, t }));
    };

    return (
        <>
            <Form
                layout="inline"
                className="flex flex-wrap md:flex-nowrap gap-1"
                onFinish={onFinish}
                form={form}
                autoComplete='off'
            >
                <Form.Item<FieldType>
                    name="roomName"
                >
                    <Input placeholder="Room name" />
                </Form.Item>

                <BaseButton type="ghost" htmlType='submit'>Search</BaseButton>
            </Form>
        </>
    );
};

export default FilterRoomsPage;