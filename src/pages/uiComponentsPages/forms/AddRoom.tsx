import React, { useEffect, useState } from 'react';
import { Input, Modal, Form, Select, SelectProps, Tabs, InputNumber, message, Upload, UploadFile, } from 'antd';
import type { TabsProps } from 'antd';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import {
    doGetRoom,
    doGetRoomTypes,
    doGetRoomServiceMain,
    doGetRoomServiceSub,
    doCreateRoom,
    doUpdateRoom,
    setRoomId,
} from '@app/store/slices/roomSlice';
import { httpApi } from '@app/api/http.api';
import { deleteImageByName } from '@app/store/slices/imageSlice';
import { PlusOutlined } from '@ant-design/icons';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';

interface AddRoomProps {
    isModalOpen?: boolean;
    setIsModalOpen?: (state: boolean) => void;
    hotelId?: string;
    language?: string;
    roomId?: string;
}

interface FileItem {
    uid: string | number;
    name: string;
    status: string;
    url: string;
}

export const AddRoom: React.FC<AddRoomProps> = ({ isModalOpen, setIsModalOpen, hotelId, language, roomId }) => {
    const [form] = Form.useForm();
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const { room, roomTypes, roomServiceMain, roomServiceSub } = useSelector((state: any) => state.room);
    const [fileList, setFileList] = React.useState<FileItem[]>([]);

    useEffect(() => {
        dispatch(doGetRoomTypes({}));
        dispatch(doGetRoomServiceMain({}));
        dispatch(doGetRoomServiceSub({}));
    }, []);

    useEffect(() => {
        if (roomId) {
            dispatch(doGetRoom({ values: { roomId }, t }));
        }
    }, [roomId]);

    useEffect(() => {
        if (roomId && room) {
            const roomInfoVI = room?.roomInfos.find((info: any) => info.languageCode === "VI");
            const roomInfoEN = room?.roomInfos.find((info: any) => info.languageCode === "EN");
            form.setFieldsValue({
                nameVI: roomInfoVI?.name,
                descriptionVI: roomInfoVI?.description,
                languageCodeVI: roomInfoVI?.languageCode,
                nameEN: roomInfoEN?.name,
                descriptionEN: roomInfoEN?.description,
                languageCodeEN: roomInfoEN?.languageCode,
                roomType: room?.type.id,
                acreage: room?.acreage,
                quantityRoom: room?.quantityRoom,
                price: room?.price,
                capacity: room?.capacity,
                adultQuantity: room?.adultQuantity,
                childrenQuantity: room?.childrenQuantity,
                singleBedQuantity: room?.singleBedQuantity,
                twinBedQuantity: room?.twinBedQuantity,
                extraBedQuantity: room?.extraBedQuantity,
                servicesMain: room?.services.map((service: any) => {
                    return service.type === 'MAIN' ? service.id : undefined;
                }).filter(Boolean) || [],
                servicesSub: room?.services.map((service: any) => {
                    return service.type === 'SUB' ? service.id : undefined;
                }).filter(Boolean) || [],
            });
        }
        else {
            form.resetFields();
            form.setFieldsValue({
                languageCodeVI: 'VI',
                languageCodeEN: 'EN',
            });
        }
        if (room?.images) {
            setFileList(
                room.images?.map((image: any, index: number) => ({
                    uid: String(index),
                    name: image,
                    status: 'done',
                    url: process.env.REACT_APP_IMAGE_URL + image,
                }))
            );
        }
        else {
            setFileList([]);
        }
    }, [roomId, room, isModalOpen]);

    const showModal = () => {
        if (setIsModalOpen) {
            setIsModalOpen(true);
        }
        form.resetFields();
    };

    const handleOk = () => {
        if (setIsModalOpen) {
            setIsModalOpen(false);
        }
        form.resetFields();
    };

    const handleCancel = () => {
        if (setIsModalOpen) {
            setIsModalOpen(false);
        }
        form.resetFields();
        dispatch(setRoomId(''));
    };

    const onFinish = (values: any) => {
        console.log('Success:', values);
        values.roomInfoRequests = [
            {
                name: values.nameVI,
                description: values.descriptionVI,
                languageCode: values.languageCodeVI,
            },
            {
                name: values.nameEN,
                description: values.descriptionEN,
                languageCode: values.languageCodeEN,
            },
        ];
        values.roomServiceCatalogIds = [
            ...values.servicesMain,
            ...values.servicesSub,
        ];
        values.roomTypeId = values.roomType;
        values.roomId = roomId;
        values.hotelId = hotelId;
        if (setIsModalOpen) {
            dispatch(roomId ? doUpdateRoom({ values, t }) : doCreateRoom({ values, t }));
            setIsModalOpen(false);
        }
        form.resetFields();
        dispatch(setRoomId(''));
    };

    // Upload
    const handleDeleteImage = (name: string) => {
        dispatch(deleteImageByName(name));
        setFileList(
            fileList.filter((file: any) => file.name !== name)
        );
    }

    const uploadButton = (
        <button style={{ border: 0, background: 'none' }} type="button">
            <PlusOutlined />
            <div style={{ marginTop: 8 }}>Upload</div>
        </button>
    );

    // Select
    const serviceCodeToName: Record<string, string> = {
        // Main
        "standing_shower": "Standing Shower",
        "fridge": "Fridge",
        "tea": "Tea",
        "air_conditional": "Air Conditional",
        "tivi": "Tivi",
        "coffee": "Coffee",
        "water": "Water",
        "gym": "Gym",
        "fruit": "Fruit",
        // Sub
        "free_breakfast": "Free Breakfast",
        "free_lunch": "Free Lunch",
        "free_dinner": "Free Dinner",
        "free_lunch_for_child": "Free Lunch For Child",
        "pet_care_service": "Pet Care Service",
        "body_care": "Body Care",
        "babysit": "Babysit",
    };
    const optionsServiceMain: SelectProps['options'] = [];
    if (roomServiceMain) {
        roomServiceMain.forEach((serviceMain: any) => {
            optionsServiceMain.push({
                label: serviceCodeToName[serviceMain.code],
                value: serviceMain.id
            })
        })
    }

    const optionsServiceSub: SelectProps['options'] = [];
    if (roomServiceSub) {
        roomServiceSub.forEach((serviceSub: any) => {
            optionsServiceSub.push({
                label: serviceCodeToName[serviceSub.code],
                value: serviceSub.id
            })
        })
    }

    const handleBlur = (fieldName: string) => (e: React.FocusEvent<HTMLInputElement>) => {
        const { value } = e.target;
        const trimmedValue = value.trim();
        form.setFieldsValue({ [fieldName]: trimmedValue });
    };

    const items: TabsProps['items'] | any = [
        {
            key: '1',
            label: 'Thông tin phòng',
            children: (
                <Form
                    form={form}
                    onFinish={onFinish}
                    layout="vertical"
                    name="form_in_modal"
                    initialValues={{ modifier: 'public' }}
                >
                    <div className='grid grid-cols-2 gap-3'>
                        <div>
                            <div className='flex gap-3'>
                                <Form.Item
                                    name="nameVI"
                                    label="Room Name"
                                    className='w-8/12'
                                    rules={[{ required: true, message: 'Please input the hotel name!' }]}
                                >
                                    <Input
                                        onBlur={handleBlur('nameVI')}
                                    />
                                </Form.Item>
                                <Form.Item
                                    name="languageCodeVI"
                                    label="Language"
                                    className='w-4/12'
                                    rules={[{ required: true, message: 'Please input field!' }]}
                                >
                                    <Select
                                        options={[
                                            { value: 'VI', label: 'Việt Nam' },
                                            { value: 'EN', label: 'English' },
                                        ]}
                                        disabled
                                    />
                                </Form.Item>
                            </div>
                            <Form.Item
                                name="descriptionVI"
                                label="Description"
                            >
                                <Input.TextArea
                                    style={{ height: '90px' }}
                                />
                            </Form.Item>
                        </div>
                        <div>
                            <div className='flex gap-3'>
                                <Form.Item
                                    name="nameEN"
                                    label="Room Name"
                                    className='w-8/12'
                                    rules={[{ required: true, message: 'Please input the hotel name!' }]}
                                >
                                    <Input
                                        onBlur={handleBlur('nameEN')}
                                    />
                                </Form.Item>
                                <Form.Item
                                    name="languageCodeEN"
                                    label="Language"
                                    className='w-4/12'
                                    rules={[{ required: true, message: 'Please input field!' }]}

                                >
                                    <Select
                                        options={[
                                            { value: 'VI', label: 'Việt Nam' },
                                            { value: 'EN', label: 'English' },
                                        ]}
                                        disabled
                                    />
                                </Form.Item>
                            </div>
                            <Form.Item
                                name="descriptionEN"
                                label="Description"
                            >
                                <Input.TextArea
                                    style={{ height: '90px' }}
                                />
                            </Form.Item>
                        </div>
                    </div>
                    <div className='grid grid-cols-2 gap-3'>
                        <Form.Item
                            name="roomType"
                            label="Type"
                            rules={[{ required: true, message: 'Please input the address!' }]}
                        >
                            <Select>
                                {
                                    roomTypes?.map((type: any) => (
                                        <Select.Option key={type.id} value={type.id}>
                                            {type.code}
                                        </Select.Option>
                                    ))
                                }
                            </Select>
                        </Form.Item>
                        <Form.Item
                            name="price"
                            label="Price"
                            rules={[{ required: true, message: 'Please input the price!' }]}
                        >
                            <InputNumber
                                type="number"
                                controls={false}
                                style={{
                                    width: '100%',
                                }}
                            />
                        </Form.Item>
                    </div>
                    <div className='grid grid-cols-4 gap-3'>
                        <Form.Item
                            name="acreage"
                            label="Acreage"
                            rules={[{ required: true, message: 'Please input the acreage!' }]}
                        >
                            <InputNumber
                                type="number"
                                controls={false}
                                style={{
                                    width: '100%',
                                }}
                            />
                        </Form.Item>
                        <Form.Item
                            name="quantityRoom"
                            label="Room"
                            rules={[{ required: true, message: 'Please input the quantity room!' }]}
                        >
                            <InputNumber
                                type="number"
                                controls={false}
                                style={{
                                    width: '100%',
                                }}
                            />
                        </Form.Item>
                        <Form.Item
                            name="capacity"
                            label="Capacity"
                            rules={[{ required: true, message: 'Please input the capacity!' }]}
                        >
                            <InputNumber
                                type="number"
                                controls={false}
                                style={{
                                    width: '100%',
                                }}
                            />
                        </Form.Item>
                        <Form.Item
                            name="adultQuantity"
                            label="Adult"
                            rules={[{ required: true, message: 'Please input the adult quantity!' }]}
                        >
                            <InputNumber
                                type="number"
                                controls={false}
                                style={{
                                    width: '100%',
                                }}
                            />
                        </Form.Item>
                        <Form.Item
                            name="childrenQuantity"
                            label="Children"
                            rules={[{ required: true, message: 'Please input the children quantity!' }]}
                        >
                            <InputNumber
                                type="number"
                                controls={false}
                                style={{
                                    width: '100%',
                                }}
                            />
                        </Form.Item>
                        <Form.Item
                            name="singleBedQuantity"
                            label="Single Bed"
                            rules={[{ required: true, message: 'Please input the single bed quantity!' }]}
                        >
                            <InputNumber
                                type="number"
                                controls={false}
                                style={{
                                    width: '100%',
                                }}
                            />
                        </Form.Item>
                        <Form.Item
                            name="twinBedQuantity"
                            label="Twin Bed"
                            rules={[{ required: true, message: 'Please input the twin bed quantity!' }]}
                        >
                            <InputNumber
                                type="number"
                                controls={false}
                                style={{
                                    width: '100%',
                                }}
                            />
                        </Form.Item>
                        <Form.Item
                            name="extraBedQuantity"
                            label="Extra Bed"
                            rules={[{ required: true, message: 'Please input the extra bed quantity!' }]}
                        >
                            <InputNumber
                                type="number"
                                controls={false}
                                style={{
                                    width: '100%',
                                }}
                            />
                        </Form.Item>
                    </div>
                    <Form.Item
                        name="servicesMain"
                        label="Services Main"
                        rules={[{ required: true, message: 'Please input the extra bed quantity!' }]}
                    >
                        <Select
                            mode="multiple"
                            allowClear
                            style={{ width: '100%' }}
                            placeholder="Please select"
                            options={optionsServiceMain}
                        />
                    </Form.Item>
                    <Form.Item
                        name="servicesSub"
                        label="Services Sub"
                        rules={[{ required: true, message: 'Please input the extra bed quantity!' }]}
                    >
                        <Select
                            mode="multiple"
                            allowClear
                            style={{ width: '100%' }}
                            placeholder="Please select"
                            options={optionsServiceSub}
                        />
                    </Form.Item>
                    <PermissionChecker
                        hasPermissions={['room_create_info', 'room_update_info']}
                    >
                        <Form.Item>
                            <div className="flex justify-center items-center gap-4">
                                <BaseButton type='ghost' onClick={handleCancel}>
                                    Cancel
                                </BaseButton>
                                <BaseButton type="primary" htmlType="submit">
                                    Save
                                </BaseButton>
                            </div>
                        </Form.Item>
                    </PermissionChecker>
                </Form>
            )
        },
        roomId ?
            {
                key: '2',
                label: 'Thư viện ảnh',
                children: (
                    <Form
                        form={form}
                        onFinish={onFinish}
                        layout="vertical"
                        name="form_in_modal"
                        initialValues={{ modifier: 'public' }}
                    >
                        <PermissionChecker
                            hasPermissions={['hotel_update_info']}
                        >
                            <Form.Item
                                label="Images"
                                name="images"
                                rules={[{ required: true, message: 'Please input your images!' }]}
                            >
                                <Upload
                                    action={
                                        (file: any) => {
                                            return new Promise((resolve, reject) => {
                                                const formData = new FormData();
                                                if (roomId) {
                                                    formData.append('images', file);
                                                    formData.append('targetId', roomId);
                                                }
                                                httpApi.post('/admin/v1/images', formData)
                                                    .then(({ data }) => {
                                                        if (data) {
                                                            message.success('Upload image success');
                                                            setFileList((prevFileList: any[]) => [
                                                                ...prevFileList,
                                                                ...data.data.map((image: string, index: number) => ({
                                                                    uid: String(index),
                                                                    name: image,
                                                                    status: 'done',
                                                                    url: process.env.REACT_APP_IMAGE_URL + image,
                                                                }))
                                                            ])
                                                            resolve("Success");
                                                        }
                                                    }).catch
                                                    (error => {
                                                        message.error('Upload image failed');
                                                        reject(error);
                                                    });
                                            });
                                        }}
                                    beforeUpload={(file: File) => {
                                        const acceptedTypes = ['image/jpeg', 'image/png'];
                                        const isFileTypeAccepted = acceptedTypes.includes(file.type);
                                        if (!isFileTypeAccepted) {
                                            message.error('Only JPG and PNG files are allowed!');
                                        }
                                        return isFileTypeAccepted;
                                    }}
                                    onRemove={(file: any) => {
                                        handleDeleteImage(file.name);
                                    }}
                                    headers={
                                        {
                                            'Content-Type': 'multipart/form-data',
                                        }
                                    }
                                    maxCount={10}
                                    fileList={fileList as UploadFile<any>[]}
                                    multiple
                                    listType="picture-card"
                                >
                                    {fileList.length >= 10 ? null : uploadButton}
                                </Upload>
                            </Form.Item>
                        </PermissionChecker>

                    </Form>
                ),
            }
            : undefined
    ]

    return (
        <Modal
            title={roomId ? 'Edit Room' : 'Add Room'}
            open={isModalOpen}
            onOk={onFinish}
            onCancel={handleCancel}
            footer={null}
            width={1200}
        >
            <Tabs defaultActiveKey="1" items={items} tabPosition='left' />
        </Modal>
    );
};

export default AddRoom;
