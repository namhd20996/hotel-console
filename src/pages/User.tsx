import React, { useEffect, useState } from 'react'
import { Input, Checkbox, Table, Modal, Switch, Form } from 'antd';
import { ClusterOutlined, EditOutlined, HomeOutlined, LockOutlined, MailOutlined, PhoneOutlined, ReloadOutlined, SettingOutlined, UserOutlined } from '@ant-design/icons';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { useDispatch, useSelector } from 'react-redux';
import { doGetUsers, doAddUser, doActiveUser, doDeleteUser } from '@app/store/slices/userSlice';
import { PermissionChecker } from '@app/components/auth/PermissionChecker/PermissionChecker';
import { Link, useNavigate, useLocation } from "react-router-dom";
import { useTranslation } from 'react-i18next';


const User = () => {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  const params = new URLSearchParams(location.search).get('active') || 'ACTIVE';
  const { users, permissions, loading, totalPages, currentPage, limitPage } = useSelector((state: any) => state.user);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);

  useEffect(() => {
    permissions.forEach((element: { permissionCode: string }) => {
      (element.permissionCode === 'admin_read' || element.permissionCode === 'admin_master') && dispatch(doGetUsers({ currentPage, limitPage, status: params, t }));
    });

  }, [dispatch, currentPage, limitPage]);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleBlur = (fieldName: string) => (e: React.FocusEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const trimmedValue = value.trim();
    form.setFieldsValue({ [fieldName]: trimmedValue });
  };

  const onFinish = (values: any) => {
    dispatch(doAddUser({ values, t }));
    setIsModalOpen(false);
    form.resetFields();
    setIsButtonDisabled(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    form.resetFields();
    setIsButtonDisabled(true);
  };

  const onChangeChecked = (value: boolean) => {
    const status = value ? 'ACTIVE' : 'NO_ACTIVE';

    const queryParams = new URLSearchParams(location.search);
    queryParams.set('active', status);
    const params = queryParams.get('active');
    console.log(params, 'params')

    navigate(`${location.pathname}?${queryParams.toString()}`);

    dispatch(doGetUsers({ currentPage, limitPage, status: params, t: t }));
  };

  const dataSource = users.map((user: any, index: number) => {
    return {
      key: -(- (currentPage - 1) * limitPage - (index + 1)),
      id: user?.userId,
      fullName: user?.fullName,
      email: user?.email,
      phone: user?.phoneNumber,
      status: user?.isDeleted
    }
  });

  const columns = [
    {
      title: 'STT',
      dataIndex: 'key',
      key: 'key',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: t('user.fullname'),
      dataIndex: 'fullName',
      key: 'fullName',
    },

    {
      title: t('user.phone'),
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: t('user.status'),
      dataIndex: 'status',
      key: 'status',
      render: (text: string, record: any) => (
        <span className='flex items-center justify-between'>
          {record.status === false ? (
            <span className="ms-3 text-base font-bold text-[#52c41a] dark:text-gray-300">{t('user.active')}</span>
          ) : (
            <span className="ms-3 text-base font-bold text-[#f5222d] dark:text-gray-300">{t('user.in_active')}</span>
          )}
          <PermissionChecker
            hasPermissions={['admin_update']}
          >
            <Switch
              className={`ml-2 ${record.status ? 'bg-gray-500' : 'bg-blue-500'}`}
              checked={!!record.status ? false : true}
              onChange={(e) => {
                if (!!e) {
                  dispatch(doActiveUser({ id: record.id, t: t }));
                } else {
                  dispatch(doDeleteUser({ id: record.id, t: t }));
                }
              }}
              loading={loading}
            />
          </PermissionChecker>
        </span>
      ),
    },
    {
      title: t('user.action'),
      key: 'action',

      render: (text: any, record: any) => (
        <div className='flex gap-5'>
          <PermissionChecker
            hasPermissions={['admin_write']}
          >
            <Link to={`user-group/${record.id}`}>
              <BaseButton
                type="ghost"
                icon={<ClusterOutlined />}
              />
            </Link>
          </PermissionChecker>
          {/* <BaseButton
            type="primary"
            icon={<EditOutlined />}
          /> */}
        </div>
      ),
    }
  ];

  return (
    <div>
      <h3 className='text-2xl font-bold capitalize'>{t('user.user')}</h3>
      <div>
        <h6 className="text-base text-sky-700">{t('user.find')}</h6>
        <div className="flex flex-wrap md:flex-nowrap items-center">
          <div className="w-full">
            <Input placeholder={t('user.input.placeholder_user')} />
          </div>
          <div className="w-full px-3">
            <Checkbox
              checked={params === 'ACTIVE' ? true : false}
              onChange={(e) => onChangeChecked(e.target.checked)}
            />
            <span className="ml-2">{t('user.input.checked')}</span>
          </div>
          <div className="w-full flex flex-nowrap gap-5">
            <BaseButton type="primary" className="ml-auto">{t('user.button.find_user')}</BaseButton>
            <div className='border-solid border-[1px] border-zinc-400 my-3'></div>
            <PermissionChecker
              hasPermissions={['admin_write']}
            >
              <BaseButton type="default" onClick={showModal}>{t('user.button.add_user')}</BaseButton>
              <div className='border-solid border-[1px] border-zinc-400 my-3'></div>
            </PermissionChecker>
            <ReloadOutlined
              className='text-2xl'
              onClick={() => dispatch(doGetUsers({ currentPage, limitPage, t }))}
            />
          </div>
        </div>
        <PermissionChecker
          hasPermissions={['admin_read']}
        >
          <Table
            className='mt-4'
            dataSource={dataSource}
            columns={columns}
            loading={loading}
            pagination={{
              showSizeChanger: true,
              pageSizeOptions: ['5', '10', '20', '30', '40', '50'],
              defaultPageSize: limitPage,
              defaultCurrent: currentPage,
              total: totalPages * limitPage,
              onChange: (page: number, pageSize: number) => {
                dispatch(doGetUsers({ currentPage: page, limitPage: pageSize, t: t }));
              },
            }} />
        </PermissionChecker>
      </div>
      <Modal
        title={t('user.modal.title')}
        open={isModalOpen}
        onOk={onFinish}
        onCancel={handleCancel}
        footer={null}
      >
        <Form
          name="user-form"
          onFinish={onFinish}
          layout='vertical'
          form={form}
          onValuesChange={(e) => {
            if (form.getFieldValue('fullName') && form.getFieldValue('email') && form.getFieldValue('phoneNumber') && form.getFieldValue('password') && form.getFieldValue('confirm')) {
              form.validateFields()
                .then((data) => {
                  setIsButtonDisabled(false)
                })
                .catch((err: any) => {
                  if (err.errorFields.length > 0)
                    setIsButtonDisabled(true)
                  else
                    setIsButtonDisabled(false)
                })
            }
          }}
        >
          <Form.Item
            name="fullName"
            rules={[
              {
                required: true,
                message: t('user.modal.message.user_message'),
              },
            ]}
          >
            <Input
              allowClear={true}
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder={t('user.modal.input.placeholder_user')}
              onBlur={handleBlur('fullName')}
            />
          </Form.Item>
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                message: t('user.modal.message.email_message1'),
              },
              {
                type: 'email',
                message: t('user.modal.message.email_message2'),
              },
            ]}
          >
            <Input
              allowClear={true}
              prefix={<MailOutlined className="site-form-item-icon" />}
              placeholder="Email"
              onKeyDown={(e) => {
                if (e.key === ' ') {
                  e.preventDefault();
                }
                if (e.key === '+') {
                  e.preventDefault();
                }
              }}
              onBlur={handleBlur('email')}
            />
          </Form.Item>
          <Form.Item
            name="phoneNumber"
            rules={[
              {
                required: true,
                message: t('user.modal.message.phone_message'),
              },
              {
                pattern: /^(\+[0-9]{11}|0[0-9]{9})$/,
                message: t('user.modal.message.phone_message1'),
              },
            ]}
          >
            <Input
              allowClear={true}
              prefix={<PhoneOutlined className="site-form-item-icon" />}
              placeholder={t('user.modal.input.placeholder_phone')}
              onKeyDown={(e) => {
                if (e.key === ' ') {
                  e.preventDefault();
                }
              }}
              onChange={(e) => {
                const value = e.target.value;
                const filteredValue = value.replace(/[^0-9+]/g, '');

                form.setFieldsValue({ phoneNumber: filteredValue });
              }}
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              //password has to have at least one uppercase, one lowercase, one number and one special character
              {
                validator: (_, value) => {
                  if (!value) {
                    return Promise.reject(t('user.modal.message.password_message'));
                  } else if (value.length < 8) {
                    return Promise.reject(t('user.modal.message.password_message1'));
                  } else if (!/(?=.*[A-Z])/.test(value)) {
                    return Promise.reject(t('user.modal.message.password_message2'));
                  } else if (!/(?=.*[a-z])/.test(value)) {
                    return Promise.reject(t('user.modal.message.password_message3'));
                  } else if (!/(?=.*\d)/.test(value)) {
                    return Promise.reject(t('user.modal.message.password_message4'));
                  } else if (!/(?=.*[^\da-zA-Z])/.test(value)) {
                    return Promise.reject(t('user.modal.message.password_message5'));
                  }
                  return Promise.resolve();
                },
              },
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder={t('user.modal.input.placeholder_password')}
              onKeyDown={(e) => {
                if (e.key === ' ') {
                  e.preventDefault();
                }
              }}
              onBlur={handleBlur('password')}
            />
          </Form.Item>
          <Form.Item
            name="confirm"
            dependencies={['password']}
            rules={[
              {
                required: true,
                message: t('user.modal.message.password_retype_massager'),
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error(t('user.modal.message.password_retype_massager2')));
                },
              }),
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder={t('user.modal.input.placeholder_password_retype')}
              onKeyDown={(e) => {
                if (e.key === ' ') {
                  e.preventDefault();
                }
              }}
              onBlur={handleBlur('confirm')}
            />
          </Form.Item>

          <Form.Item>
            <div className="flex justify-center items-center gap-4">
              <BaseButton type='ghost' onClick={handleCancel}>
                {t('user.modal.button.cancel')}
              </BaseButton>
              <BaseButton type="primary" htmlType="submit" disabled={isButtonDisabled}>
                {t('user.modal.button.save')}
              </BaseButton>
            </div>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  )
}

export default User