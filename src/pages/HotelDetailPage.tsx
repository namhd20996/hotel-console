import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { doGetHotel, doCreateHotel, doUpdateHotel, doGetServicesAmenity, doGetServicesLocation, doGetProvinces } from '@app/store/slices/hotelSlice';
import { deleteImageByName } from '@app/store/slices/imageSlice';
import { Checkbox, Form, Input, InputNumber, Rate, Select, SelectProps, Upload, UploadFile, message } from 'antd';
import { BaseButton } from '@app/components/common/BaseButton/BaseButton';
import { PlusOutlined } from '@ant-design/icons';
import { httpApi } from '@app/api/http.api';
import PermissionChecker from '@app/components/auth/PermissionChecker/PermissionChecker';

interface HotelDetailPageProps {
}

interface FileItem {
    uid: string | number;
    name: string;
    status: string;
    url: string;
}

export const HotelDetailPage: React.FC<HotelDetailPageProps> = (props) => {
    const { id } = useParams();
    const [form] = Form.useForm();
    const navigate = useNavigate();
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const { hotel, servicesAmenity, servicesLocation, languageCode, provinces } = useSelector((state: any) => state.hotel);
    const [fileList, setFileList] = React.useState<FileItem[]>([]);

    React.useEffect(() => {
        if (id) {
            dispatch(doGetHotel({ id, languageCode }));
        }
        else {
            form.resetFields();
            form.setFieldsValue({
                currency: 'VND',
            });
        }
        dispatch(doGetServicesAmenity({ languageCode }));
        dispatch(doGetServicesLocation({ languageCode }));
        dispatch(doGetProvinces({ languageCode }))
    }, [dispatch, id]);

    React.useEffect(() => {
        if (hotel && id) {
            form.setFieldsValue(
                {
                    hotelName: hotel.hotelInfo.name,
                    address: hotel.hotelInfo.address,
                    city: hotel.hotelInfo.city,
                    currency: hotel.currency,
                    languageCode: hotel.hotelInfo.languageCode,
                    status: hotel.hotelInfo.status,
                    star: hotel.star,
                    price: hotel.price,
                    description: hotel.hotelInfo.description,
                    services: hotel.services,
                    images: hotel.images,
                    serviceAmenity: hotel.services?.map((service: any) => {
                        return service.type === 'AMENITY' ? service.id : undefined;
                    }).filter(Boolean) || [],

                    serviceLocation: hotel.services?.map((service: any) => {
                        return service.type === 'LOCATION' ? service.id : undefined;
                    }).filter(Boolean) || [],
                }
            );
            if (hotel.images) {
                setFileList(
                    hotel.images.map((image: any, index: number) => ({
                        uid: String(index),
                        name: image,
                        status: 'done',
                        url: process.env.REACT_APP_IMAGE_URL + image,
                    }))
                );
            }
            else {
                setFileList([]);
            }
        }
    }, [hotel, form]);

    const optionsAmenity: SelectProps['options'] = [];
    if (servicesAmenity) {
        servicesAmenity.forEach((serviceAmenity: any) => {
            optionsAmenity.push({
                label: serviceAmenity.code,
                value: serviceAmenity.id
            })
        })
    }

    const optionsLocation: SelectProps['options'] = [];
    if (servicesLocation) {
        servicesLocation.forEach((serviceLocation: any) => {
            optionsLocation.push({
                label: serviceLocation.code,
                value: serviceLocation.id
            })
        })
    }

    const handleChange = (value: string[]) => {
        // console.log(`selected ${value}`);
    };

    const onFinish = async (values: any) => {
        const data = {
            star: values.star,
            price: values.price,
            currency: values.currency,
            serviceCatalogIds: [
                ...values.serviceAmenity || [],
                ...values.serviceLocation || [],
            ],
            hotelInfoRequest: {
                name: values.hotelName,
                address: values.address,
                city: values.city,
                description: values.description,
                languageCode: values.languageCode,
            },
            images: fileList.map((file: any) => file.name),
        };
        if (id) {
            await dispatch(doUpdateHotel({ values: { id, ...data }, t }));
        }
        else {
            await dispatch(doCreateHotel({ values: { ...data }, t }));
        }
        await navigate('/hotels');
    };

    const handleDeleteImage = (name: string) => {
        dispatch(deleteImageByName(name));
        setFileList(
            fileList.filter((file: any) => file.name !== name)
        );
    }

    const uploadButton = (
        <button style={{ border: 0, background: 'none' }} type="button">
            <PlusOutlined />
            <div style={{ marginTop: 8 }}>Upload</div>
        </button>
    );

    const handleBlur = (fieldName: string) => (e: React.FocusEvent<HTMLInputElement>) => {
        const { value } = e.target;
        const trimmedValue = value.trim();
        form.setFieldsValue({ [fieldName]: trimmedValue });
    };

    type FieldType = {
        hotelName?: string;
        address?: string;
        city?: string;
        description?: string;
        currency?: string;
        languageCode?: string;
        star?: number;
        price?: number;
        serviceAmenity?: string[];
        serviceLocation?: string[];
        images?: any;
    };

    return (
        <>
            <div className="flex justify-between item-center">

                <h3 className='text-2xl font-bold capitalize'>Hotel Detail</h3>
                <Link to='/hotels' className='text-blue-500'>Back to hotels</Link>
            </div>
            <Form
                name="basic"
                form={form}
                layout='vertical'
                // initialValues={{ remember: true }}
                onFinish={onFinish}
                autoComplete="off"
            >
                <Form.Item<FieldType>
                    label={t('detailshotel.hotelName')}
                    name="hotelName"
                    rules={[{ required: true, message: 'Please input your hotel name!' }]}
                >
                    <Input
                        onBlur={handleBlur('hotelName')}
                    />
                </Form.Item>
                <div className="flex gap-3">
                    <Form.Item<FieldType>
                        className="w-4/5"
                        label={t('detailshotel.address')}
                        name="address"
                        rules={[{ required: true, message: 'Please input your address!' }]}
                    >
                        <Input
                            onBlur={handleBlur('address')}
                        />
                    </Form.Item>
                    <Form.Item<FieldType>
                        className="w-1/5"
                        label={t('detailshotel.city')}
                        name="city"
                        rules={[{ required: true, message: 'Please input your city!' }]}
                    >
                        <Select
                            showSearch
                        >
                            {
                                provinces.map((province: any) => (
                                    <Select.Option key={province.provinceId} value={province.province}>{province.province}</Select.Option>
                                ))
                            }
                        </Select>
                    </Form.Item>
                </div>
                <div className="flex gap-3">
                    <Form.Item<FieldType>
                        className="w-1/4"
                        label={t('detailshotel.language')}
                        name="languageCode"
                        rules={[{ required: true, message: 'Please input your language!' }]}
                    >
                        <Select>
                            <Select.Option value="VI">Việt Nam</Select.Option>
                            <Select.Option value="EN">English</Select.Option>
                        </Select>
                    </Form.Item>

                    <Form.Item<FieldType>
                        className="w-1/4"
                        label={t('detailshotel.currency')}
                        name="currency"
                        rules={[{ required: true, message: 'Please input your currency!' }]}
                    >
                        <Input disabled />
                    </Form.Item>

                    <Form.Item<FieldType>
                        className="w-1/4"
                        label={t('detailshotel.price')}
                        name="price"
                        rules={[{ required: true, message: 'Please input your price!' }]}
                    >
                        <InputNumber className='w-full' />
                    </Form.Item>

                    <Form.Item<FieldType>
                        className="w-1/4"
                        label={t('detailshotel.star')}
                        name="star"
                        rules={[{ required: true, message: 'Please input your star!' }]}
                    >
                        <Rate style={{ fontSize: "30px" }} />
                    </Form.Item>
                </div>
                {
                    id && (
                        <PermissionChecker
                            hasPermissions={['hotel_update_info']}
                        >
                            <Form.Item<FieldType>
                                label="Images"
                                name="images"
                                rules={[{ required: true, message: 'Please input your images!' }]}>
                                <Upload
                                    action={
                                        (file: any) => {
                                            return new Promise((resolve, reject) => {
                                                const formData = new FormData();
                                                if (id) {
                                                    formData.append('images', file);
                                                    formData.append('targetId', id);
                                                }
                                                httpApi.post('/admin/v1/images', formData)
                                                    .then(({ data }) => {
                                                        if (data) {
                                                            message.success('Upload image success');
                                                            setFileList((prevFileList: any[]) => [
                                                                ...prevFileList,
                                                                ...data.data.map((image: string, index: number) => ({
                                                                    uid: String(index),
                                                                    name: image,
                                                                    status: 'done',
                                                                    url: process.env.REACT_APP_IMAGE_URL + image,
                                                                }))
                                                            ])
                                                            resolve("Success");
                                                        }
                                                    }).catch
                                                    (error => {
                                                        message.error('Upload image failed');
                                                        reject(error);
                                                    });
                                            });
                                        }}
                                    beforeUpload={(file: File) => {
                                        const acceptedTypes = ['image/jpeg', 'image/png'];
                                        const isFileTypeAccepted = acceptedTypes.includes(file.type);
                                        if (!isFileTypeAccepted) {
                                            message.error('Only JPG and PNG files are allowed!');
                                        }
                                        return isFileTypeAccepted;
                                    }}
                                    onRemove={(file: any) => {
                                        handleDeleteImage(file.name);
                                    }}
                                    headers={
                                        {
                                            'Content-Type': 'multipart/form-data',
                                        }
                                    }
                                    maxCount={10}
                                    fileList={fileList as UploadFile<any>[]}
                                    multiple
                                    listType="picture-card"
                                >
                                    {fileList.length >= 10 ? null : uploadButton}
                                </Upload>
                            </Form.Item>
                        </PermissionChecker>
                    )
                }

                <Form.Item<FieldType>
                    label={t('detailshotel.description')}
                    name="description"
                // rules={[{ required: true, message: 'Please input your description!' }]}
                >
                    <Input.TextArea />
                </Form.Item>

                <Form.Item<FieldType>
                    label={t('detailshotel.amenity')}
                    name="serviceAmenity"
                // rules={[{ required: true, message: 'Please input your service amenity!' }]}
                >
                    <Select
                        mode="multiple"
                        allowClear
                        style={{ width: '100%' }}
                        placeholder={t('detailshotel.pleaseSelect')}
                        onChange={handleChange}
                        options={optionsAmenity}
                    />
                </Form.Item>

                <Form.Item<FieldType>
                    label={t('detailshotel.location')}
                    name="serviceLocation"
                // rules={[{ required: true, message: 'Please input your service location!' }]}
                >
                    <Select
                        mode="multiple"
                        allowClear
                        style={{ width: '100%' }}
                        placeholder={t('detailshotel.pleaseSelect')}
                        onChange={handleChange}
                        options={optionsLocation}
                    />
                </Form.Item>

                <PermissionChecker
                    hasPermissions={['hotel_update_info']}
                >
                    <Form.Item>
                        <BaseButton type="primary" htmlType="submit">
                            {
                                id ? 'Update' : 'Create'
                            }
                        </BaseButton>
                    </Form.Item>
                </PermissionChecker>

            </Form>
        </>
    );

};

export default HotelDetailPage;
