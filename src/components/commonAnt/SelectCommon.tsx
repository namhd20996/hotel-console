import React from 'react';
import { Select, Space } from 'antd';

interface SelectCommonProps {
  data: { value: string; label: string; disabled?: boolean }[];
  onSetSelected: (value: string) => void;
}

const SelectCommon: React.FC<SelectCommonProps> = ({ data, onSetSelected }) => {
  const handleChange = (value: string) => {
    onSetSelected(value);
  };

  return (
    <Space wrap>
      <Select
        placeholder="Select group"
        defaultValue={data[1]?.value}
        style={{ width: 160 }}
        allowClear
        options={data}
        onChange={handleChange}
      />
    </Space>
  );
};

export default SelectCommon;
