# Hotel CMS repo

`YARN IS REQUIRED!`

Development mode
```
yarn install && yarn start
```

Production mode
```
yarn install && yarn build
```

#### How to analyze the bundle size
```
yarn install && yarn build --stats
```



And then use the [webpack-bundle-analyzer](https://www.npmjs.com/package/webpack-bundle-analyzer) to open _build/bundle-stats.json_.

---

Sometimes your issue is not matching the node version
> npm install --force
> yarn install
> yarn start