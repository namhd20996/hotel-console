FROM node:16
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install --force
COPY . .
RUN YARN_IGNORE_NODE=1 yarn build


# Expose the port that the application runs on
EXPOSE 3000

# Command to run the application
CMD ["yarn", "start"]