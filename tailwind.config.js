/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js, ts, tsx}"],
  theme: {
    extend: {
      backgroundColor: {
        bgColorBlueDark: "rgb(4, 133, 253)",
      },
      colors: {
        colorBlueMain: "rgb(80, 170, 254)",
      },
    },
  },
  plugins: [],
}